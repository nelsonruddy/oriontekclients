package com.oriontek.challenge.controller.request;

import com.sun.istack.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AddressRequestSaveModel {

    @NotNull
    private String address;




}
