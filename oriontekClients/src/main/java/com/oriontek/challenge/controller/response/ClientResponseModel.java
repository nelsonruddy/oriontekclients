package com.oriontek.challenge.controller.response;

import com.oriontek.challenge.repository.domain.Address;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class ClientResponseModel {

    private String name;

    private String lastName;

    private String phone;

    private String age;

    List<Address> address;
}
