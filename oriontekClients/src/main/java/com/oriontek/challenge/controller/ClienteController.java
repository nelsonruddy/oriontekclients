package com.oriontek.challenge.controller;

import static org.springframework.http.HttpStatus.OK;

import com.oriontek.challenge.controller.request.ClientRequestSaveModel;
import com.oriontek.challenge.controller.response.ClientResponseModel;
import com.oriontek.challenge.repository.domain.Client;
import com.oriontek.challenge.service.ClientService;
import java.io.InvalidObjectException;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/cliente")
@RequiredArgsConstructor
public class ClienteController {

    private final ClientService clientService;

    @PostMapping
    @ResponseStatus(OK)
    public ClientResponseModel createClient(@RequestBody ClientRequestSaveModel clientSave)
        throws InvalidObjectException {

        return  clientService.createClient(clientSave);
    }

    @GetMapping
    @ResponseStatus(OK)
    public List<Client> getAllClient(){

        return clientService.getAllClient();

    }

}