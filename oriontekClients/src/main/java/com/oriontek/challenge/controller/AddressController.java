package com.oriontek.challenge.controller;

import static org.springframework.http.HttpStatus.OK;

import com.oriontek.challenge.controller.request.AddressRequestSaveModel;
import com.oriontek.challenge.controller.request.ClientRequestSaveModel;
import com.oriontek.challenge.controller.response.AddressResponseModel;
import com.oriontek.challenge.controller.response.ClientResponseModel;
import com.oriontek.challenge.service.AddressService;
import com.oriontek.challenge.service.ClientService;
import java.io.InvalidObjectException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @PostMapping
    @ResponseStatus(OK)
    public AddressResponseModel createAddress(@RequestBody AddressRequestSaveModel addressSave) {

        return  addressService.createAddress(addressSave);

    }


}
