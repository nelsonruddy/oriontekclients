package com.oriontek.challenge.controller.request;

import com.oriontek.challenge.repository.domain.Address;
import com.sun.istack.NotNull;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ClientRequestSaveModel {

    @NotNull
    private String name;
    @NotNull
    private String lastName;
    @NotNull
    private String phone;
    @NotNull
    private String age;

    List<Address> address;

}
