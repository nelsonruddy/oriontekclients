package com.oriontek.challenge.repository.domain;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "Client")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "Name",nullable = true)
    private String name;
    @Column(name = "Last_Name",nullable = true)
    private String lastName;
    @Column(name = "Phone",nullable = true)
    private String phone;
    @Column(name = "Age",nullable = true)
    private String age;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "address_id")
    List<Address> address;

}
