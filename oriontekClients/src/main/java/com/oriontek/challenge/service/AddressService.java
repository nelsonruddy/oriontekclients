package com.oriontek.challenge.service;

import com.oriontek.challenge.controller.request.AddressRequestSaveModel;
import com.oriontek.challenge.controller.response.AddressResponseModel;

public interface AddressService {

    AddressResponseModel createAddress(AddressRequestSaveModel addressRequestSaveModel);


}
