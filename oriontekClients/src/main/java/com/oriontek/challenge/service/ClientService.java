package com.oriontek.challenge.service;

import com.oriontek.challenge.controller.request.ClientRequestSaveModel;
import com.oriontek.challenge.controller.response.ClientResponseModel;
import com.oriontek.challenge.repository.domain.Client;
import java.util.List;


public interface ClientService {

    ClientResponseModel createClient(ClientRequestSaveModel clientRequestSaveModel);

    List<Client>  getAllClient();
}
