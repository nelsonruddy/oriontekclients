package com.oriontek.challenge.service.implementation;


import com.oriontek.challenge.controller.request.ClientRequestSaveModel;
import com.oriontek.challenge.controller.response.ClientResponseModel;
import com.oriontek.challenge.repository.AddressRepository;
import com.oriontek.challenge.repository.ClientRepository;

import com.oriontek.challenge.repository.domain.Address;
import com.oriontek.challenge.repository.domain.Client;
import com.oriontek.challenge.service.ClientService;

import java.util.List;
import javax.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    private final AddressRepository addressRepository;


    @Override
    public ClientResponseModel createClient(ClientRequestSaveModel clientRequestSaveModel) {

        clientRequestSaveModel.getAddress().forEach(validar -> {
            findById(validar.getId());
        });




        Client cliente = new Client();

        cliente.setName(clientRequestSaveModel.getName());
        cliente.setLastName(clientRequestSaveModel.getLastName());
        cliente.setPhone(clientRequestSaveModel.getPhone());
        cliente.setAge(clientRequestSaveModel.getAge());
        cliente.setAddress(clientRequestSaveModel.getAddress());
        clientRepository.save(cliente);

        return ClientResponseModel.builder()
            .name(clientRequestSaveModel.getName())
            .lastName(clientRequestSaveModel.getLastName())
            .phone(clientRequestSaveModel.getPhone())
            .age(clientRequestSaveModel.getAge())
            .address(clientRequestSaveModel.getAddress())
            .build();

    }

    @Override
    public List<Client> getAllClient() {

       return clientRepository.findAll();

    }

    private void findById(Long id){
        Address address = addressRepository.findById(id).orElseThrow(
            () -> new EntityNotFoundException("not found")
        );
    }

}
