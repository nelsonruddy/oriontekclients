package com.oriontek.challenge.service.implementation;

import com.oriontek.challenge.controller.request.AddressRequestSaveModel;
import com.oriontek.challenge.controller.request.ClientRequestSaveModel;
import com.oriontek.challenge.controller.response.AddressResponseModel;
import com.oriontek.challenge.controller.response.ClientResponseModel;
import com.oriontek.challenge.repository.AddressRepository;
import com.oriontek.challenge.repository.ClientRepository;
import com.oriontek.challenge.repository.domain.Address;
import com.oriontek.challenge.repository.domain.Client;
import com.oriontek.challenge.service.AddressService;
import com.oriontek.challenge.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;


    @Override
    public AddressResponseModel createAddress(AddressRequestSaveModel addressRequestSaveModel) {

        Address address = new Address();
        address.setAddress(addressRequestSaveModel.getAddress());

        addressRepository.save(address);

        return  AddressResponseModel.builder()
            .address(addressRequestSaveModel.getAddress())
            .build();
    }
}
