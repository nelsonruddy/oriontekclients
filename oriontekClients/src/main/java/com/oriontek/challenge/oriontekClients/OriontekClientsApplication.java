package com.oriontek.challenge.oriontekClients;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OriontekClientsApplication {

	public static void main(String[] args) {
		SpringApplication.run(OriontekClientsApplication.class, args);
	}

}
